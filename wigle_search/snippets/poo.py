
from pygle import config, network

my_ssid = 'prettyflyforawifi'


net_results = network.search (ssid = my_ssid, first = 0, latrange1 = 30.0,\
								latrange2 = 50.0, longrange1 = -85.0, longrange2 = -120.0)

for result in net_results[u'results']:
	print result[u'ssid'], result[u'netid'], result[u'trilat'],\
	result[u'trilong'], result[u'lastupdt']