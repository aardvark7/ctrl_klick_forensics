from pygle import config, network
import sys, json, simplekml, sqlite3
from geopy.geocoders import Nominatim

locale = raw_input ('Enter a place name: ')
if locale == '':
	geo_limit = False
else:
	geo_limit = True

#get some stuff from the user (ssid, max number of results, details or not)	
net_ssid = raw_input ('Enter the SSId of your network: ')
max_results = int(raw_input ('Enter the maximum number of search results: '))
if raw_input ('Do you want details on each network? (y/n)') == 'y':
	get_details = True
else:
	get_details = False

def get_bounding_box(loc_name):
	geolocator = Nominatim()
	location = geolocator.geocode(loc_name, language = 'en')
	coords = [float(x) for x in location.raw[u'boundingbox']]
	return coords
	

def oui_vendor(mac):
	conn = sqlite3.connect('macvendors.db')
	cur = conn.cursor()
		
	oui = mac.replace(':', '').replace('-', '').upper()[:-6]
	cur.execute ('SELECT vendor FROM macvendors WHERE mac="{a}"'.format(a=oui))
	records = cur.fetchall()
	try:
		vendor = records[0][0]
	except IndexError:
		vendor = "No Vendor Found"
	conn.close()
	return vendor


#instantiate a new KML object
#after you create a kml, you can export/save as a kmz so your icons and pics will go along with it
kml = simplekml.Kml()

#just setting places to save stuff
path = ''
fname = net_ssid + '.kml' #output filename will be based off the ssid entered

#different simple searches
#net_results = network.search(resultsPerPage=999, ssid=net_ssid, latrange1=37.3, latrange2=37.90, longrange1=-112.90, longrange2=-113.20)
count = 0

if geo_limit:
	box = get_bounding_box(locale)

f_out = open(net_ssid + '.tsv', 'wb')
f_out.write('BSSID\tSSID\tLatitude\tLongitude\tFirst Time\tLast Time\tLast Update\tVendor\n')

bssid_list = []

for i in range (max_results / 100):

	if geo_limit:
		net_results = network.search(resultsPerPage=999, ssidlike=net_ssid, first=i * 100,\
						latrange1 = box[0], latrange2 = box[1], longrange1 = box[2], longrange2 = box[3])
	else:
		net_results = network.search(resultsPerPage=999, ssidlike=net_ssid, first=i * 100)
		
	if net_results[u'resultCount'] == 0:
		break

	#loop through results and make a KML point for each one
		
	for result in net_results[u'results']:
		count += 1
		pnt = kml.newpoint(name = result[u'ssid'], coords = [(float(result[u'trilong']), float(result[u'trilat']))])
		pnt.style.iconstyle.icon.href = 'wifi4.png' #point this at a logo you want to use. Could be URL as well
		pnt.description = result[u'netid']
		print count, "BSSID: %s\tFirst Time: %s\tLast Time: %s\tLast Update: %s\tCoords: (%s, %s)" % (result[u'netid'], result[u'firsttime'], result[u'lasttime'], result[u'lastupdt'], result[u'trilat'], result[u'trilong'])
		bssid_list.append(result[u'netid'])
		#I know, I know, "stick to one kind of string formatter!" FU. If Python didn't want me to use it, they'd take it out
		f_out.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (result[u'netid'], result['ssid'], result[u'trilat'], result[u'trilong'], result[u'firsttime'], result[u'lasttime'], result[u'lastupdt'], oui_vendor(result[u'netid'])))
	
#save kml with filename and path stuff	
kml.save(path + fname)

print "Your SSID, {a}, was found {b} times.".format (a=net_ssid, b=count)
f_out.close()


if get_details:
	for bssid in bssid_list:
		net_count = 0
		kml_details = simplekml.Kml()
		net_results = network.detail(netid=bssid)

#loop through results and make a KML point for each one
		if net_results[u'success']:
			for result in net_results[u'results'][0][u'locationData']:
				pnt = kml_details.newpoint(name = result[u'ssid'], coords = [(float(result[u'longitude']), float(result[u'latitude']))])
				pnt.description = result[u'time']
				pnt.style.iconstyle.icon.href = 'wifi4.png' #point this at a logo you want to use. could be URL
				net_count += 1

#save kml with filename and path stuff	
		if net_count !=0:
			kml_details.save(bssid.replace(':', '-') + '.kml')
			print "For the BSSID: " + bssid + ", found %d results." % net_count
		else:
			print 'Sorry, did\'nt find anything.'
			
