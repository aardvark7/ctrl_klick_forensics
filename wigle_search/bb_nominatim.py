#take an input and produce bounding box using Nominatim from OpenStreetMap
#this makes a kml file with the box in a transparent red color

from geopy.geocoders import Nominatim
import sys
import simplekml


geolocator = Nominatim()

#take named location directly from command line
#command form  is "python bb_nominatim.py "New York City" or...
#"python bb_nominatim.py "Brazil"

loc_name = sys.argv[1]
location = geolocator.geocode(loc_name, language = 'en')

#get bounding_box (geo_box) from location.raw
#this gives [South Latitude, North Latitude, West Longitude, East Longitude]
#for other things we need two corner points (SLat, WLong, NLat, ELong)

geo_box = location.raw[u'boundingbox']
twit_order = [0, 2, 1, 3]
twit_geo_box = [float(geo_box[i]) for i in twit_order]

#print to test it
print location.raw[u'display_name'].decode('utf-8')
print [float(x) for x in location.raw[u'boundingbox']]

#output to kml to test it

kml = simplekml.Kml()
b_box = kml.newgroundoverlay (name=loc_name)
b_box.color = '371400FF' #this is transparent red
b_box.latlonbox.north = twit_geo_box[2]
b_box.latlonbox.south = twit_geo_box[0]
b_box.latlonbox.east = twit_geo_box[3]
b_box.latlonbox.west = twit_geo_box[1]

#save kml file with name based on the full location name
kml.save (str(location).replace(', ', '-').replace(' ', '_') + '_bounding_box.kml')
