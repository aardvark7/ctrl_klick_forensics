from pygle import config, network
import sys, json, simplekml, sqlite3

def oui_vendor(mac):
	oui = mac.replace(':', '').replace('-', '').upper()[:-6]
	cur.execute ('SELECT {a} FROM {b} WHERE {c}="{d}"'.format(a=vendor_col, b=table_name, c=mac_col, d=oui))
	records = cur.fetchall()
	try:
		return records[0][0]
	except IndexError:
		return "No Vendor Found"

table_name = 'macvendors'
vendor_col = 'vendor'
mac_col = 'mac'

conn = sqlite3.connect('macvendors.db')
cur = conn.cursor()


#instantiate a new KML object
#after you create a kml, you can export/save as a kmz so your icons and pics will go along with it
kml = simplekml.Kml()

#just setting places to save stuff
path = ''
net_bssid = sys.argv[1] #take the name of the ssid from the command line directly
max_results = int(sys.argv[2]) #max results entered via command line
fname = net_bssid.replace(':', '-') + '.kml' #output filename will be based off the ssid entered

#different simple searches
#net_results = network.search(resultsPerPage=999, ssid=net_ssid, latrange1=37.3, latrange2=37.90, longrange1=-112.90, longrange2=-113.20)
count = 0

f_out = open(net_bssid.replace(':', '-') + '.tsv', 'wb')
f_out.write('BSSID\tSSID\tLatitude\tLongitude\tFirst Time\tLast Time\tLast Update\tVendor\n')

for i in range (max_results / 100):

	#net_results = network.search(resultsPerPage=999, ssidlike=net_ssid, \
	#	latrange1 = 36.997968, latrange2 = 42.0015016, longrange1 = -114.0528823, longrange2 = -109.0415767, first=i * 100)
	net_results = network.search(resultsPerPage=999, netid = net_bssid, first=i * 100)
	if net_results[u'resultCount'] == 0:
		break
	#net_results = network.search(resultsPerPage=999, netid="66:5A:3A")

	#loop through results and make a KML point for each one
	bssid_list = []
	
	for result in net_results[u'results']:
		count += 1
		pnt = kml.newpoint(name = result[u'ssid'], coords = [(float(result[u'trilong']), float(result[u'trilat']))])
		pnt.style.iconstyle.icon.href = 'wifi4.png' #point this at a logo you want to use. Could be URL as well
		pnt.description = result[u'netid']
		print count, "SSID: %s\tFirst Time: %s\tLast Time: %s\tLast Update: %s\tCoords: (%s, %s)" % (result[u'ssid'], result[u'firsttime'], result[u'lasttime'], result[u'lastupdt'], result[u'trilat'], result[u'trilong'])
		bssid_list.append(result[u'netid'])
		#I know, I know, "stick to one kind of string formatter!" FU. If Python didn't want me to use it, they'd take it out
		f_out.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (result[u'netid'], result['ssid'], result[u'trilat'], result[u'trilong'], result[u'firsttime'], result[u'lasttime'], result[u'lastupdt'], oui_vendor(result[u'netid'])))
	
#save kml with filename and path stuff	
kml.save(path + fname)

print "Your OUI, %s, was found %d times." % (net_bssid, count)
f_out.close()