import sqlite3, sys, datetime, json


hydra_db = sys.argv[1]
wigle_db = sys.argv[2]

con_hydra = sqlite3.Connection(hydra_db)
con_wigle = sqlite3.Connection(wigle_db)

cur_hydra = con_hydra.cursor()
cur_wigle = con_wigle.cursor()

bluetooths = cur_hydra.execute('SELECT address, name, vendor, company, classic_rssi, le_rssi FROM blue_hydra_devices;')


for bt in bluetooths:

	if bt[5] != None:
		timestamps = json.loads(bt[5])
		for time in timestamps:
			lower = (int(time['t']) - 3) * 1000
			upper = (int(time['t']) + 3) * 1000
			date = cur_wigle.execute('SELECT lat, lon, time FROM location WHERE time BETWEEN {a} AND {b};'.format(a=lower, b=upper))
			ts_location = date.fetchone()
			#print ts_location
			if ts_location != None:
				print "LE\t", bt[0], time['t'], time['rssi'], str(ts_location[2]), str(ts_location[0]), str(ts_location[1])
			else:
				print "LE\t", bt[0], time['t'], time['rssi']
				
	elif bt[4] != None:
		timestamps = json.loads(bt[4])
		for time in timestamps:
			lower = (int(time['t']) - 3) * 1000
			upper = (int(time['t']) + 3) * 1000
			date = cur_wigle.execute('SELECT lat, lon, time FROM location WHERE time BETWEEN {a} AND {b};'.format(a=lower, b=upper))
			ts_location = date.fetchone()
			if ts_location != None:
				print "Classic\t", bt[0], time['t'], time['rssi'], str(ts_location[2]), str(ts_location[0]), str(ts_location[1])
			else:
				print "Classic\t", bt[0], time['t'], time['rssi']

con_hydra.close()
con_wigle.close()

