import sys, webbrowser, simplekml, sqlite3, os
from subprocess import Popen
from pygle import network, config
from PyQt4 import QtCore, QtGui, uic
from PyQt4.Qt import *
from django.utils.encoding import smart_unicode
from geopy.geocoders import Nominatim

qtCreatorFile = "wigle_search.ui" # Enter file here.

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

class MyApp(QtGui.QMainWindow, Ui_MainWindow):
	def __init__(self):
		QtGui.QMainWindow.__init__(self)
		Ui_MainWindow.__init__(self)
		self.setupUi(self)
		QtGui.QApplication.setStyle(QtGui.QStyleFactory.create('plastique'))
		self.wigle_web_button.clicked.connect(self.loadWigleWeb)
		self.nominatim_button.clicked.connect(self.bbNominatim)
		self.net_search_button.clicked.connect(self.networkSearch)
		self.actionwigle_web_site.triggered.connect(self.loadWigleWeb)
		self.actionExit.triggered.connect(self.closeApplication)
		self.wigle_web_button.clicked.connect(self.loadWigleWeb)
		self.show_bounding_box.clicked.connect(self.showBoundingBox)
		
	def loadWigleWeb(self):
		webbrowser.open('https://wigle.net', new=0, autoraise=True)
		
	def closeApplication(self):
		choice = QtGui.QMessageBox.question(self, 'Exit', 'Exit the Application?', QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
		
		if choice == QtGui.QMessageBox.Yes:
			sys.exit()
		else:
			pass
		
	def bbNominatim(self):
		geolocator = Nominatim()
		location_name = self.nominatim_entry.text().toUtf8()
		location = geolocator.geocode(location_name, language = 'en')
		try:
			geo_box = location.raw[u'boundingbox']
			self.lat1_entry.setText (geo_box[0].encode('utf-8'))
			self.lat2_entry.setText (geo_box[1].encode('utf-8'))
			self.long1_entry.setText (geo_box[2].encode('utf-8'))
			self.long2_entry.setText (geo_box[3].encode('utf-8'))
			self.nominatim_entry.setText(smart_unicode(location.raw[u'display_name']))
		except AttributeError:
			self.nominatim_entry.setText('No location found. Maybe it\'s you.')
			self.popupWindow('Location Not Found', 'I can\'t find that location. Maybe you can try going back in time and learning how to spell.')

	def showBoundingBox(self):
		if (self.lat1_entry.text() != "" and self.lat2_entry.text() != ""\
			and self.long1_entry.text() != "" and self.long2_entry.text() != ""):
			kml = simplekml.Kml()
			b_box = kml.newgroundoverlay (name=smart_unicode(self.nominatim_entry.text()))
			b_box.color = '371400FF' #this is transparent red
			b_box.latlonbox.north = float(self.lat2_entry.text())
			b_box.latlonbox.south = float(self.lat1_entry.text())
			b_box.latlonbox.east = float(self.long2_entry.text())
			b_box.latlonbox.west = float(self.long1_entry.text())
			
			#save kml file with name based on the full location name
			kml_filename = smart_unicode(self.nominatim_entry.text()).replace(', ', '-').replace(' ', '_') + '_bounding_box.kml'
			kml.save (kml_filename)
			Popen('"C:\Program Files (x86)\Google\Google Earth Pro\client\googleearth.exe" ' + os.path.realpath(kml_filename), stdin=None, stdout=None, stderr=None, close_fds=True)
		else:
			self.popupWindow("No Coordinates For Box", "You are missing one or more coordinates for your bounding box. Try searching a location to populate lat/long values.")
			

			
			
			
			
			
			
	def ouiVendor(self, mac):
		conn = sqlite3.connect('macvendors.db')
		cur = conn.cursor()
		
		oui = mac.replace(':', '').replace('-', '').upper()[:-6]
		cur.execute ('SELECT vendor FROM macvendors WHERE mac="{a}"'.format(a=oui))
		records = cur.fetchall()
		try:
			vendor = records[0][0]
		except IndexError:
			vendor = "No Vendor Found"
		conn.close()
		return vendor
	
	def popupWindow(self, title, message):
		self.msg = QMessageBox()
		self.msg.setIcon(QMessageBox.Information)
		self.msg.setWindowTitle(title)
		self.msg.setText(message)
		self.msg.setStandardButtons(QMessageBox.Ok)
		self.msg.exec_()
		
	def networkSearch(self):
		kml = simplekml.Kml()

		#just setting places to save stuff
		path = ''
		net_ssid = smart_unicode (self.ssid_entry.text())
		fname = net_ssid + '.kml' #output filename will be based off the ssid entered
		count = 0
		f_out = open(net_ssid + '.tsv', 'wb')
		f_out.write('BSSID\tSSID\tLatitude\tLongitude\tFirst Time\tLast Time\tLast Update\tVendor\n')
		
		bssid_list = []

		for i in range (self.max_results.value() / 100):

			if self.geo_check.isChecked():
				net_results = network.search(resultsPerPage=999, ssidlike=net_ssid, first=i * 100,\
						latrange1 = float(self.lat1_entry.text()), latrange2 = float(self.lat2_entry.text()), longrange1 = float(self.long1_entry.text()), longrange2 = float(self.long2_entry.text()))
			else:
				net_results = network.search(resultsPerPage=999, ssidlike=net_ssid, first=i * 100)
			
			if net_results[u'resultCount'] == 0:
				break

			#loop through results and make a KML point for each one
		
			for result in net_results[u'results']:
				count += 1
				pnt = kml.newpoint(name = result[u'ssid'], coords = [(float(result[u'trilong']), float(result[u'trilat']))])
				pnt.style.iconstyle.icon.href = 'wifi4.png' #point this at a logo you want to use. Could be URL as well
				pnt.description = result[u'netid']
				#print count, "BSSID: %s\tFirst Time: %s\tLast Time: %s\tLast Update: %s\tCoords: (%s, %s)" % (result[u'netid'], result[u'firsttime'], result[u'lasttime'], result[u'lastupdt'], result[u'trilat'], result[u'trilong'])
				bssid_list.append(result[u'netid'])
				#I know, I know, "stick to one kind of string formatter!" FU. If Python didn't want me to use it, they'd take it out
				f_out.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (result[u'netid'], result['ssid'], result[u'trilat'], result[u'trilong'], result[u'firsttime'], result[u'lasttime'], result[u'lastupdt'], self.ouiVendor(result[u'netid'])))
		
			kml.save(path + fname)

		self.popupWindow("Success!", "Your SSID, {a}, was found {b} times.".format (a=net_ssid, b=count))
		f_out.close()


		if self.details_check.isChecked():
			for bssid in bssid_list:
				net_count = 0
				kml_details = simplekml.Kml()
				net_results = network.detail(netid=bssid)

		#loop through results and make a KML point for each one
				if net_results[u'success']:
					for result in net_results[u'results'][0][u'locationData']:
						pnt = kml_details.newpoint(name = result[u'ssid'], coords = [(float(result[u'longitude']), float(result[u'latitude']))])
						pnt.description = result[u'time']
						pnt.style.iconstyle.icon.href = 'wifi4.png' #point this at a logo you want to use. could be URL
				net_count += 1
		
		
		
		
		
			

if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	window = MyApp()
	window.show()
	sys.exit(app.exec_())
	
