from pygle import config, network
import sys, simplekml

#instantiate a new KML object
kml = simplekml.Kml()

#just setting places to save stuff
net_bssid = sys.argv[1] #take the name of the ssid from the command line directly
fname = net_bssid.replace(":", "-") + '.kml' #output filename will be based off the ssid entered

print net_bssid

net_count = 0
#different simple searches

net_results = network.detail(netid=net_bssid)

#loop through results and make a KML point for each one
if net_results[u'success']:
	for result in net_results[u'results'][0][u'locationData']:
		pnt = kml.newpoint(name = result[u'ssid'], coords = [(float(result[u'longitude']), float(result[u'latitude']))])
		pnt.description = result[u'time']
		pnt.style.iconstyle.icon.href = 'wifi4.png' #point this at a logo you want to use. could be URL
		net_count += 1

#save kml with filename and path stuff	
if net_count !=0:
	kml.save(fname)
	print "Found %d results." % net_count
else:
	print 'Sorry, didn\'t find anything.'