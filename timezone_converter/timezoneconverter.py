import pandas as pd
import sys, datetime
from PyQt4 import QtCore, QtGui, uic

qtCreatorFile = "timezoneconverter.ui" # Enter ui file here.  This is your GUI


Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile) #Needed to load .ui file


class Time_Converter(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        
        #These connect buttons in the GUI to the differant functions
        self.comboBox_3.activated[str].connect(self.input_tz)
        self.comboBox_4.activated[str].connect(self.output_tz)
        self.pushButton.clicked.connect(self.ConvertTime)
        self.pushButton_2.clicked.connect(QtCore.QCoreApplication.instance().quit) #closes the program
   
    # Changes Combo Boxes to display a different list of options based on the Time Zone Region   
    def input_tz(self):
        US = ("Select Region Time Zone", "Alaska","Aleutian", "Arizona", "Central", "Eastern", "East-Indiana", "Hawaii", "Indiana-Starke", "Michigan", "Mountain","Pacific", "Samoa")
        UTC = ("Select Region Time Zone","UTC")
        Etc = ("Select Region Time Zone","GMT","GMT+0","GMT+1","GMT+10","GMT+11","GMT+12","GMT+2","GMT+3","GMT+4","GMT+5","GMT+6","GMT+7","GMT+8","GMT+9","GMT-0","GMT-1","GMT-10","GMT-11","GMT-12","GMT-13","GMT-14","GMT-2","GMT-3","GMT-4","GMT-5","GMT-6","GMT-7","GMT-8","GMT-9","GMT0")
        Canada = ("Select Region Time Zone", "Atlantic", "Central", "East-Saskatchewan", "Eastern", "Mountain","Newfoundland", "Pacific", "Saskatchewan", "Yukon", "Continental", "EasterIsland")
        Australia = ("Select Region Time Zone","ACT", "Adelaide","Brisbane","Broken_Hill","Canberra","Currie","Darwin","Eucla","Hobart","LHI","Lindeman","Lord_Howe","Melbourne","NSW","North","Perth","Queensland","South","Sydney","Tasmania","Victoria","West","Yancowinna")
        Europe = ("Select Region Time Zone","Podgorica","Prague","Riga","Rome","Samara","San_Marino","Sarajevo","Simferopol","Skopje","Sofia","Stockholm","Tallinn","Tirane","Tiraspol","Uzhgorod","Vaduz","Vatican","Vienna","Vilnius","Volgograd","Warsaw","Zagreb","Zaporozhye","Zurich")
        Africa = ("Select Region Time Zone","Abidjan","Accra","Addis_Ababa","Algiers","Asmara","Asmera","Bamako","Bangui","Banjul","Bissau","Blantyre","Brazzaville","Bujumbura","Cairo","Casablanca","Ceuta","Conakry","Dakar","Dar_es_Salaam","Djibouti","Douala","El_Aaiun","Freetown","Gaborone","Harare","Johannesburg","Juba","Kampala","Khartoum","Kigali","Kinshasa","Lagos","Libreville","Lome","Luanda","Lubumbashi","Lusaka","Malabo","Maputo","Maseru","Mbabane","Mogadishu","Monrovia","Nairobi","Ndjamena","Niamey","Nouakchott","Ouagadougou","Porto-Novo","Sao_Tome","Timbuktu","Tripoli","Tunis","Windhoek")
        Asia = ("Select Region Time Zone","Aden","Almaty","Amman","Anadyr","Aqtau","Aqtobe","Ashgabat","Ashkhabad","Baghdad","Bahrain","Baku","Bangkok","Beirut","Bishkek","Brunei","Calcutta","Choibalsan","Chongqing","Chungking","Colombo","Dacca","Damascus","Dhaka","Dili","Dubai","Dushanbe","Gaza","Harbin","Hebron","Ho_Chi_Minh","Hong_Kong","Hovd","Irkutsk","Istanbul","Jakarta","Jayapura","Jerusalem","Kabul","Kamchatka","Karachi","Kashgar","Kathmandu","Katmandu","Kolkata","Krasnoyarsk","Kuala_Lumpur","Kuching","Kuwait","Macao","Macau","Magadan","Makassar","Manila","Muscat","Nicosia","Novokuznetsk","Novosibirsk","Omsk","Oral","Phnom_Penh","Pontianak","Pyongyang","Qatar","Qyzylorda","Rangoon","Riyadh","Saigon","Sakhalin","Samarkand","Seoul","Shanghai","Singapore","Taipei","Tashkent","Tbilisi","Tehran","Tel_Aviv","Thimbu","Thimphu","Tokyo","Ujung_Pandang","Ulaanbaatar","Ulan_Bator","Urumqi","Vientiane","Vladivostok","Yakutsk","Yekaterinburg","Yerevan")
        NZ = ("Select Region Time Zone","NZ")
        Mexico = ("Select Region Time Zone","BajaNorte","BajaSur","General")
        
        intzregion = str(self.comboBox_3.currentText())

        if intzregion == "US":
            self.comboBox.clear()
            self.comboBox.addItems(US)
        elif intzregion == "UTC":
            self.comboBox.clear()
            self.comboBox.addItems(UTC)
        elif intzregion == "Australia":
            self.comboBox.clear()
            self.comboBox.addItems(Australia)            
        elif intzregion == "Etc":
            self.comboBox.clear()
            self.comboBox.addItems(Etc)
        elif intzregion == "Canada":
            self.comboBox.clear()
            self.comboBox.addItems(Canada)
        elif intzregion == "Europe":
            self.comboBox.clear()
            self.comboBox.addItems(Europe)
        elif intzregion == "Africa":
            self.comboBox.clear()
            self.comboBox.addItems(Africa)
        elif intzregion == "Asia":
            self.comboBox.clear()
            self.comboBox.addItems(Asia)
        elif intzregion == "NZ":
            self.comboBox.clear()
            self.comboBox.addItems(NZ)
        elif intzregion == "Mexico":
            self.comboBox.clear()
            self.comboBox.addItems(Mexico)             
                
    def output_tz(self):
        US = ("Select Region Time Zone", "Alaska","Aleutian", "Arizona", "Central", "Eastern", "East-Indiana", "Hawaii", "Indiana-Starke", "Michigan", "Mountain","Pacific", "Samoa")
        UTC = ("Select Region Time Zone","UTC")
        Etc = ("Select Region Time Zone","GMT","GMT+0","GMT+1","GMT+10","GMT+11","GMT+12","GMT+2","GMT+3","GMT+4","GMT+5","GMT+6","GMT+7","GMT+8","GMT+9","GMT-0","GMT-1","GMT-10","GMT-11","GMT-12","GMT-13","GMT-14","GMT-2","GMT-3","GMT-4","GMT-5","GMT-6","GMT-7","GMT-8","GMT-9","GMT0")
        Canada = ("Select Region Time Zone", "Atlantic", "Central", "East-Saskatchewan", "Eastern", "Mountain","Newfoundland", "Pacific", "Saskatchewan", "Yukon", "Continental", "EasterIsland")
        Australia = ("Select Region Time Zone","ACT", "Adelaide","Brisbane","Broken_Hill","Canberra","Currie","Darwin","Eucla","Hobart","LHI","Lindeman","Lord_Howe","Melbourne","NSW","North","Perth","Queensland","South","Sydney","Tasmania","Victoria","West","Yancowinna")
        Europe = ("Select Region Time Zone","Podgorica","Prague","Riga","Rome","Samara","San_Marino","Sarajevo","Simferopol","Skopje","Sofia","Stockholm","Tallinn","Tirane","Tiraspol","Uzhgorod","Vaduz","Vatican","Vienna","Vilnius","Volgograd","Warsaw","Zagreb","Zaporozhye","Zurich")
        Africa = ("Select Region Time Zone","Abidjan","Accra","Addis_Ababa","Algiers","Asmara","Asmera","Bamako","Bangui","Banjul","Bissau","Blantyre","Brazzaville","Bujumbura","Cairo","Casablanca","Ceuta","Conakry","Dakar","Dar_es_Salaam","Djibouti","Douala","El_Aaiun","Freetown","Gaborone","Harare","Johannesburg","Juba","Kampala","Khartoum","Kigali","Kinshasa","Lagos","Libreville","Lome","Luanda","Lubumbashi","Lusaka","Malabo","Maputo","Maseru","Mbabane","Mogadishu","Monrovia","Nairobi","Ndjamena","Niamey","Nouakchott","Ouagadougou","Porto-Novo","Sao_Tome","Timbuktu","Tripoli","Tunis","Windhoek")
        Asia = ("Select Region Time Zone","Aden","Almaty","Amman","Anadyr","Aqtau","Aqtobe","Ashgabat","Ashkhabad","Baghdad","Bahrain","Baku","Bangkok","Beirut","Bishkek","Brunei","Calcutta","Choibalsan","Chongqing","Chungking","Colombo","Dacca","Damascus","Dhaka","Dili","Dubai","Dushanbe","Gaza","Harbin","Hebron","Ho_Chi_Minh","Hong_Kong","Hovd","Irkutsk","Istanbul","Jakarta","Jayapura","Jerusalem","Kabul","Kamchatka","Karachi","Kashgar","Kathmandu","Katmandu","Kolkata","Krasnoyarsk","Kuala_Lumpur","Kuching","Kuwait","Macao","Macau","Magadan","Makassar","Manila","Muscat","Nicosia","Novokuznetsk","Novosibirsk","Omsk","Oral","Phnom_Penh","Pontianak","Pyongyang","Qatar","Qyzylorda","Rangoon","Riyadh","Saigon","Sakhalin","Samarkand","Seoul","Shanghai","Singapore","Taipei","Tashkent","Tbilisi","Tehran","Tel_Aviv","Thimbu","Thimphu","Tokyo","Ujung_Pandang","Ulaanbaatar","Ulan_Bator","Urumqi","Vientiane","Vladivostok","Yakutsk","Yekaterinburg","Yerevan")
        NZ = ("Select Region Time Zone","NZ")
        Mexico = ("Select Region Time Zone","BajaNorte","BajaSur","General")
        
        outtzregion = str(self.comboBox_4.currentText())

        if outtzregion == "US":
            self.comboBox_5.clear()
            self.comboBox_5.addItems(US)
        elif outtzregion == "Australia":
            self.comboBox_5.clear()
            self.comboBox_5.addItems(Australia)        
        elif outtzregion == "UTC":
            self.comboBox_5.clear()
            self.comboBox_5.addItems(UTC)
        elif outtzregion == "Etc":
            self.comboBox_5.clear()
            self.comboBox_5.addItems(Etc)
        elif outtzregion == "Canada":
            self.comboBox_5.clear()
            self.comboBox_5.addItems(Canada)
        elif outtzregion == "Europe":
            self.comboBox_5.clear()
            self.comboBox_5.addItems(Europe)
        elif outtzregion == "Africa":
            self.comboBox_5.clear()
            self.comboBox_5.addItems(Africa)
        elif outtzregion == "Asia":
            self.comboBox_5.clear()
            self.comboBox_5.addItems(Asia)
        elif outtzregion == "NZ":
            self.comboBox_5.clear()
            self.comboBox_5.addItems(NZ)
        elif outtzregion == "Mexico":
            self.comboBox_5.clear()
            self.comboBox_5.addItems(Mexico)            
    
    #Main part of the program that does the conversions        
    def ConvertTime(self):
        
        time_values = str(self.plainTextEdit.toPlainText())
        time_values = time_values.replace('\n', ",").split(",")
        
        
        #Some of the Time Zones only use a one word specification and others need the region and location
        #so when they are choosed I need to do an statement to specify which ones need the one word
        
        if str(self.comboBox_3.currentText()) == 'UTC':
            input_tz = 'UTC'
        elif str(self.comboBox_3.currentText()) == 'NZ':
            input_tz = 'NZ'        
        else:
            input_tz = str(self.comboBox_3.currentText())+"/"+str(self.comboBox.currentText())
                
        if str(self.comboBox_4.currentText()) == 'UTC':
            convert_tz = 'UTC'
        elif str(self.comboBox_4.currentText()) == 'NZ':
            convert_tz = 'NZ'
        else:
            convert_tz = str(self.comboBox_4.currentText())+"/"+str(self.comboBox_5.currentText())
                
        #this set the rows when the Convert Times is pressed.  It also resets the table if you
        #hit convert times onces and then change the time to display a different converstion.
        
        self.tableWidget.setRowCount(1)
        
        #This loops through your list of times and does the converstion for each date/time       
        for time in time_values:
            if time == "":
                continue
            else:
                intime = pd.to_datetime(time)
            
            #The try except helps with errors if someone forgets to set a timezone..It give an invalid time zone 
            #response rather than crashing the program
            
            try:        
                orig_time = intime.tz_localize(input_tz) #Sets the input time  
                convert_time = orig_time.tz_convert(convert_tz) #Converts to the output time
                
                rowcount = self.tableWidget.rowCount() #Gets the current row number
                self.tableWidget.insertRow(rowcount) #adds another row
                
                #Next formats the date and time and then inserts the text into their row location
                if self.checkBox.isChecked():
                    orig_time = orig_time.strftime('%m/%d/%Y %H:%M:%S %z %Z')
                    convert_time = convert_time.strftime('%m/%d/%Y %H:%M:%S %z %Z')
                    self.tableWidget.setItem(rowcount , 0, QtGui.QTableWidgetItem(str(orig_time)))
                    self.tableWidget.setItem(rowcount , 1, QtGui.QTableWidgetItem(str(convert_time)))
                else:
                    orig_time = orig_time.strftime('%Y/%m/%d %H:%M:%S %z %Z')
                    convert_time = convert_time.strftime('%Y/%m/%d %H:%M:%S %z %Z')                    
                    self.tableWidget.setItem(rowcount , 0, QtGui.QTableWidgetItem(str(orig_time)))
                    self.tableWidget.setItem(rowcount , 1, QtGui.QTableWidgetItem(str(convert_time)))                
            
            except:
                rowcount = self.tableWidget.rowCount()
                self.tableWidget.insertRow(rowcount)            
                self.tableWidget.setItem(rowcount , 0, QtGui.QTableWidgetItem("Invaled Time Zone Selection"))
                self.tableWidget.setItem(rowcount , 1, QtGui.QTableWidgetItem("Invaled Time Zone Selection"))




if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = Time_Converter()
    window.show()
    sys.exit(app.exec_())
    
        