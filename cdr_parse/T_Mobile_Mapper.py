#ICON for Google Maps obtained from "https://icons8.com">Icon pack by Icons8

import pandas as pd
import simplekml, csv, os, sys
from subprocess import Popen
from PyQt4 import QtCore, QtGui, uic

qtCreatorFile = "TMobileCallRecords.ui" # Enter ui file here.


Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

#These connect to buttons in the GUI to the differant functions
class Map_CDR(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.Carrier.activated[str].connect(self.CDR_Type)
        self.Browse_Button_SaveF.clicked.connect(self.Browse_Button_Save) #Connects the button to the save file location by pressing the button
        self.Browse_CDR.clicked.connect(self.Open_CDR_File) #Connects the open file location so you can browse to a CDR file
        self.showtz.stateChanged.connect(self.showtZ)
        self.Map_Records.clicked.connect(self.Map_Record) #Begins the mapping and creating KML
        self.cancel.clicked.connect(QtCore.QCoreApplication.instance().quit) #closes the program
        
    # This function changes the combobox for the records type  
    def CDR_Type(self):
        
        ATT = ("Call Detail Records","NELOS")
        T_Mobile =("Call Detail Records","True Call Records")
        Sprint = ("Call Detail Records", "PCMD")
        Verizon = ("Call Detail Records",)
        
        _CDR = str(self.Carrier.currentText())

        if _CDR == "T-Mobile / Metro PCS":
            self.Record_Type.clear()
            self.Record_Type.addItems(T_Mobile)
        elif _CDR == "AT&T":
            self.Record_Type.clear()
            self.Record_Type.addItems(ATT)
        elif _CDR == "Sprint":
            self.Record_Type.clear()
            self.Record_Type.addItems(Sprint)            
        elif _CDR == "Verizon":
            self.Record_Type.clear()
            self.Record_Type.addItems(Verizon)   
   
    #This function opens the file browser and get directory you want to save your files  
    #Then displays the path in the box next to the button.
    
    def Browse_Button_Save(self):
        savedir = QtGui.QFileDialog.getExistingDirectory()
        self.Maps_Save_Loc.setText(savedir)
    
    #This function opens the file browser and gets the cdr file as variable openf.  
    #Then displays the path in the box next to the button.        
    
    def Open_CDR_File(self):
        openf = QtGui.QFileDialog.getOpenFileName()
        self._File.setText(openf)
    

    #This function displays the local time zone dropdown when the check box is selected
    def showtZ(self):
        
        def tz():
        
            US = ("Select Region Time Zone", "Alaska","Aleutian", "Arizona", "Central", "Eastern", "East-Indiana", "Hawaii", "Indiana-Starke", "Michigan", "Mountain","Pacific", "Samoa")
            UTC = ("Select Region Time Zone","UTC")
            Etc = ("Select Region Time Zone","GMT","GMT+0","GMT+1","GMT+10","GMT+11","GMT+12","GMT+2","GMT+3","GMT+4","GMT+5","GMT+6","GMT+7","GMT+8","GMT+9","GMT-0","GMT-1","GMT-10","GMT-11","GMT-12","GMT-13","GMT-14","GMT-2","GMT-3","GMT-4","GMT-5","GMT-6","GMT-7","GMT-8","GMT-9","GMT0")
            Canada = ("Select Region Time Zone", "Atlantic", "Central", "East-Saskatchewan", "Eastern", "Mountain","Newfoundland", "Pacific", "Saskatchewan", "Yukon", "Continental", "EasterIsland")
            Australia = ("Select Region Time Zone","ACT", "Adelaide","Brisbane","Broken_Hill","Canberra","Currie","Darwin","Eucla","Hobart","LHI","Lindeman","Lord_Howe","Melbourne","NSW","North","Perth","Queensland","South","Sydney","Tasmania","Victoria","West","Yancowinna")
            Europe = ("Select Region Time Zone","Podgorica","Prague","Riga","Rome","Samara","San_Marino","Sarajevo","Simferopol","Skopje","Sofia","Stockholm","Tallinn","Tirane","Tiraspol","Uzhgorod","Vaduz","Vatican","Vienna","Vilnius","Volgograd","Warsaw","Zagreb","Zaporozhye","Zurich")
            Africa = ("Select Region Time Zone","Abidjan","Accra","Addis_Ababa","Algiers","Asmara","Asmera","Bamako","Bangui","Banjul","Bissau","Blantyre","Brazzaville","Bujumbura","Cairo","Casablanca","Ceuta","Conakry","Dakar","Dar_es_Salaam","Djibouti","Douala","El_Aaiun","Freetown","Gaborone","Harare","Johannesburg","Juba","Kampala","Khartoum","Kigali","Kinshasa","Lagos","Libreville","Lome","Luanda","Lubumbashi","Lusaka","Malabo","Maputo","Maseru","Mbabane","Mogadishu","Monrovia","Nairobi","Ndjamena","Niamey","Nouakchott","Ouagadougou","Porto-Novo","Sao_Tome","Timbuktu","Tripoli","Tunis","Windhoek")
            Asia = ("Select Region Time Zone","Aden","Almaty","Amman","Anadyr","Aqtau","Aqtobe","Ashgabat","Ashkhabad","Baghdad","Bahrain","Baku","Bangkok","Beirut","Bishkek","Brunei","Calcutta","Choibalsan","Chongqing","Chungking","Colombo","Dacca","Damascus","Dhaka","Dili","Dubai","Dushanbe","Gaza","Harbin","Hebron","Ho_Chi_Minh","Hong_Kong","Hovd","Irkutsk","Istanbul","Jakarta","Jayapura","Jerusalem","Kabul","Kamchatka","Karachi","Kashgar","Kathmandu","Katmandu","Kolkata","Krasnoyarsk","Kuala_Lumpur","Kuching","Kuwait","Macao","Macau","Magadan","Makassar","Manila","Muscat","Nicosia","Novokuznetsk","Novosibirsk","Omsk","Oral","Phnom_Penh","Pontianak","Pyongyang","Qatar","Qyzylorda","Rangoon","Riyadh","Saigon","Sakhalin","Samarkand","Seoul","Shanghai","Singapore","Taipei","Tashkent","Tbilisi","Tehran","Tel_Aviv","Thimbu","Thimphu","Tokyo","Ujung_Pandang","Ulaanbaatar","Ulan_Bator","Urumqi","Vientiane","Vladivostok","Yakutsk","Yekaterinburg","Yerevan")
            NZ = ("Select Region Time Zone","NZ")
            Mexico = ("Select Region Time Zone","BajaNorte","BajaSur","General")
            
            dttz = str(self.comboBox_3.currentText())
    
            if dttz == "US":
                self.comboBox.clear()
                self.comboBox.addItems(US)
            elif dttz == "Australia":
                self.comboBox.clear()
                self.comboBox.addItems(Australia)        
            elif dttz == "UTC":
                self.comboBox.clear()
                self.comboBox.addItems(UTC)
            elif dttz == "Etc":
                self.comboBox.clear()
                self.comboBox.addItems(Etc)
            elif dttz == "Canada":
                self.comboBox.clear()
                self.comboBox.addItems(Canada)
            elif dttz == "Europe":
                self.comboBox.clear()
                self.comboBox.addItems(Europe)
            elif dttz == "Africa":
                self.comboBox.clear()
                self.comboBox.addItems(Africa)
            elif dttz == "Asia":
                self.comboBox.clear()
                self.comboBox.addItems(Asia)
            elif dttz == "NZ":
                self.comboBox.clear()
                self.comboBox.addItems(NZ)
            elif dttz == "Mexico":
                self.comboBox.clear()
                self.comboBox.addItems(Mexico)             
      
        if self.showtz.isChecked():
            self.stackedWidget.setCurrentWidget(self.page_2)
            self.comboBox_3.activated[str].connect(tz)

        else:
            self.stackedWidget.setCurrentWidget(self.page)        
   
    #This is the main function to map the records    
    def Map_Record(self):
        
        #sets several variables for the program
        savedir = str(self.Maps_Save_Loc.text())
        fname = (str(self.Beg_Search.text()) + "_" + str(self.End_Search.text())).replace("-","").replace(":","").replace("/","")
        
        
        #creates Google Maps Directory and filename
        if not os.path.exists(savedir + "\\Google Maps"):
            os.makedirs(savedir + "\\Google Maps")        
        savemapsf = (savedir + "\\Google Maps\\" + fname + ".html")
        
        openf = str(self._File.text())

        Target_Num = str(self.Target_Num.text())             
        Carrier = str(self.Carrier.currentText())
        Record_Type = str(self.Record_Type.currentText())
        
        #This set the time zone of the entered time to UTC
        
        begin_search = pd.to_datetime(str(self.Beg_Search.text())).tz_localize('UTC')
        end_search = pd.to_datetime(str(self.End_Search.text())).tz_localize('UTC')          
        
        #Records are stored in UTC if the search time is based on local time 
        #it needs to be converted to UTC to search the records        
        
        if self.showtz.isChecked():
            if str(self.comboBox_3.currentText()) == 'UTC':
                input_tz = 'UTC'
            elif str(self.comboBox_3.currentText()) == 'NZ':
                input_tz = 'NZ'        
            else:
                input_tz = str(self.comboBox_3.currentText())+"/"+str(self.comboBox.currentText())
            
            beg_time = str(self.Beg_Search.text())
            end_time = str(self.End_Search.text()) 
            
            intime = pd.to_datetime(beg_time)
            out_time = pd.to_datetime(end_time)
            
            orig_start = intime.tz_localize(input_tz) 
            begin_search = orig_start.tz_convert('UTC')
            
            orig_end = out_time.tz_localize(input_tz) 
            end_search = orig_end.tz_convert('UTC')            
                     

        
        gmapslist = []
        f = []
             
        if Carrier == "T-Mobile / Metro PCS" and Record_Type == "Call Detail Records":
            df =pd.read_excel(openf, sheet_name=0, header = 11)
            rows = df.loc[df['Date'] >= begin_search]
            rows = rows.loc[df['Date'] <= end_search]
            rows.to_csv(savedir +'\\t-mobile.csv',columns= ['Date','1st Tower Azimuth', '1st Tower LAT', '1st Tower LONG'])
            f = (savedir +'\\t-mobile.csv')
            with open(f,'rb') as csvfile:
                sreader = csv.reader(csvfile, delimiter = ",")  
                for row in sreader:
                    if row[1] == "Date" or row[3] == "" or row[4]== "":
                        continue
                    else:
                    
                        if row[2] == "":
                            langle = "0.0"
                            rangle = "360.0"
                        else:                            
                            langle = float(row[2]) - 60.0
                            if langle < 0.0:
                                langle = langle + 360.0
                            else:
                                langle
                           
                            rangle = float(row[2]) + 60.0
                            if rangle > 360.0:
                                rangle = rangle - 360.0
                            else:
                                rangle
                                                        
                            
                        gmapslist.append( str(self.Target_Num.text()) + ";" + str(row[1]) + " UTC;" + str(langle) +";" + str(rangle) + ";" + row[2] + ";" + str(float(row[3]))+','+str(float(row[4])))
                            
        elif Carrier == "T-Mobile / Metro PCS" and Record_Type == "True Call Records":
            df =pd.read_excel(openf, sheet_name=0, header = 0)
            rows = df.loc[df['Start Time'] >= begin_search]
            rows = rows.loc[df['Start Time'] <= end_search]
            rows.to_csv(savedir +'\\t-mobile.csv',columns= ['Start Time','End Latitude [degrees]', 'End Longitude [degrees]'])
            f = (savedir +'\\t-mobile.csv') 
            with open(f,'rb') as csvfile:
                sreader = csv.reader(csvfile, delimiter = ",")  
                for row in sreader:
                    if row[1] == "Start Time" or row[2] == "N/A" or row[3] == "N/A":
                        continue
                    else:
                                    
                        langle = 0.0             
                        rangle = 360.0

                        gmapslist.append( str(self.Target_Num.text()) + ";" + str(row[1]) + " UTC;" + str(langle) +";" + str(rangle) + ";" + "0" + ";" + row[2]+','+ row[3])            
        
#Creates the HTML File
        mapsf = open(savemapsf, 'wb')
        mapsf.write('''<!DOCTYPE html>\n''')
        mapsf.write('''<html>\n''')
        mapsf.write('''<head>\n''')
        mapsf.write('''<meta http-equiv="content-type" content="text/html; charset=utf-8">\n''')
        mapsf.write('''<title>Google Maps Triage CDR Mapper</title>\n''')
        mapsf.write('''<style>\n''')
        mapsf.write('''    #map {\n''')
        mapsf.write('''    height: 100%;\n''')
        mapsf.write('''    }\n''')
        mapsf.write('''\n''')
        mapsf.write('''    html, body {\n''')
        mapsf.write('''        height: 100%;\n''')
        mapsf.write('''        margin: 0;\n''')
        mapsf.write('''        padding: 0;\n''')
        mapsf.write('''    }\n''')
        mapsf.write('''    #floating-panel {\n''')
        mapsf.write('''        position: absolute;\n''')
        mapsf.write('''        top: 10px;\n''')
        mapsf.write('''        left: 35%;\n''')
        mapsf.write('''        z-index: 5;\n''')
        mapsf.write('''        background-color: #fff;\n''')
        mapsf.write('''        padding: 5px;\n''')
        mapsf.write('''        border: 1px solid #999;\n''')
        mapsf.write('''        text-align: center;\n''')
        mapsf.write('''        font-family: 'Roboto','sans-serif';\n''')
        mapsf.write('''        line-height: 30px;\n''')
        mapsf.write('''        padding-left: 10px;\n''')
        mapsf.write('''    }\n''')
        mapsf.write('''</style>\n''')        
        mapsf.write('''</head>\n''')
        mapsf.write('''<body>\n''')
        mapsf.write('''<div id="floating-panel">\n''')
        mapsf.write('''    Show/Hide Antenna: <input onclick = "initialize()" type=checkbox id="antenna" checked = "checked">\n''') 
        mapsf.write('''    Show/Hide Sector: <input onclick = "initialize()" type=checkbox id="sector" checked = "checked">\n''')
        mapsf.write('''</div> \n''')       
        #sets size of the window
        mapsf.write('''<div id="map_canvas" style="width:1000px; height:750px; margin:0 auto;"></div>\n''')        
        mapsf.write('''<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry&sensor=false"></script>\n''')
        mapsf.write('''<script type="text/javascript">\n''')
        mapsf.write('''\n''')
        mapsf.write('''var gm = google.maps;\n''')
        mapsf.write('''var map;\n''')
        mapsf.write('''var locations = [\n''')
        for location in gmapslist:
            location = location.split(";")
                                       #telephone #          Date/time           leftangle      right angle        azimuth           lat/long
            mapsf.write('      ['+'\''+location[0]+'\','+'\''+location[1]+'\','+location[2]+','+ location[3] + ','+ location[4]+ ','+ location[5]+ '''],\n''')
            center = location[5]
		
        mapsf.write('''];\n''')
        mapsf.write('''\n''')
        mapsf.write('''function initialize() {\n''')
        mapsf.write('''\n''')
        mapsf.write('''	cent = new gm.LatLng('''+ center +'''),\n''')
        mapsf.write('''	map = new gm.Map(document.getElementById('map_canvas'), {\n''')
        mapsf.write('''         mapTypeId: google.maps.MapTypeId.ROADMAP,\n''')
        mapsf.write('''         zoom: 10,\n''')
        mapsf.write('''         center: cent\n''')
        mapsf.write('''	});\n''')
        mapsf.write('''	 var marker, i;\n''')
        mapsf.write('''\n''')        
        mapsf.write('''	 for (i = 0; i < locations.length; i++) { \n''')
        mapsf.write('''     var image = "https://png.icons8.com/filled-circle/p1em/15/27ae60";\n''')
        mapsf.write('''     var marker = new google.maps.Marker({\n''')
        mapsf.write('''        position: new google.maps.LatLng(locations[i][5], locations[i][6]),\n''')
        mapsf.write('''        map: map,\n''')
        mapsf.write('''        icon: image\n''')
        mapsf.write('''      });\n''') 
        mapsf.write('''\n''')
        mapsf.write('''	 var infowindow = new google.maps.InfoWindow();\n''')
        mapsf.write('''	   google.maps.event.addListener(marker, 'click', (function(marker, i) {\n''')
        mapsf.write('''        return function() {\n''')
        mapsf.write('''          infowindow.setContent('<b>Target Number:  </b>'+locations[i][0]+\n''')    
        mapsf.write('''		  '<br><b>Date/Time:  </b>'+locations[i][1]+'<br><b>Lat/Lng:  </b>'+\n''')
        mapsf.write('''		  locations[i][5]+','+locations[i][6]);\n''')
        mapsf.write('''          infowindow.open(map, marker);\n''')
        mapsf.write('''        }\n''')
        mapsf.write('''	  })(marker, i));\n''')
        mapsf.write('''	 }\n''')     
        mapsf.write('''	 polys = [],\n''')
        mapsf.write('''	 radiusMeters = 1609.344;\n''')
        mapsf.write('''\n''')
        mapsf.write('''    for (i=0; i < locations.length; i++) {\n''')
        mapsf.write('''	       point = new gm.LatLng(locations[i][5],locations[i][6])\n''')
        mapsf.write('''        var path = getArcPath(point, radiusMeters, locations[i][2],locations[i][3]);\n''')
        mapsf.write('''        path.unshift(point);\n''')
        mapsf.write('''        path.push(point);\n''')
        mapsf.write('''        var poly = new gm.Polygon({\n''')
        mapsf.write('''            path: path,\n''')
        mapsf.write('''            map: map,\n''')
        mapsf.write('''            fillColor:'red',\n''')
        mapsf.write('''            fillOpacity:0.6\n''')
        mapsf.write('''        });\n''')
        mapsf.write('''        polys.push(poly);\n''')
        mapsf.write('''    }\n''')
        mapsf.write('''}\n''')
        mapsf.write('''\n''')
        mapsf.write('''/***\n''')
        mapsf.write('''* REQUIRES: google.maps.geometry library, via a 'libraries=geometry' parameter\n''')
        mapsf.write('''*  on url to google maps script\n''')
        mapsf.write('''* @param center must be a google.maps.LatLng object.\n''')
        mapsf.write('''* @param radiusMeters must be a number, radius in meters.\n''')
        mapsf.write('''* @param startAngle must be an integer from 0 to 360, angle at which to begin arc.\n''')
        mapsf.write('''* @param endAngle must be an integer from 0 to 360, angle at which to end arc.\n''')
        mapsf.write('''*   For a full circle, use startAngle of 0 and endAngle of 360\n''')
        mapsf.write('''*   which will create a closed path.\n''')
        mapsf.write('''* @param direction -optional- defaults to clockwise,\n''')
        mapsf.write('''*   pass string 'counterclockwise' to reverse direction.\n''')
        mapsf.write('''* @Returns array of google.maps.LatLng objects.\n''')
        mapsf.write('''***/\n''')
        mapsf.write('''function getArcPath(center, radiusMeters, startAngle, endAngle, direction){\n''')
        mapsf.write('''    var point, previous,\n''')
        mapsf.write('''        atEnd = false,\n''')
        mapsf.write('''        points = Array(),\n''')
        mapsf.write('''        a = startAngle;\n''')
        mapsf.write('''    while (true) {\n''')
        mapsf.write('''        point = google.maps.geometry.spherical.computeOffset(center, radiusMeters, a);\n''')
        mapsf.write('''        points.push(point);\n''')
        mapsf.write('''        if (a == endAngle){\n''')
        mapsf.write('''            break;\n''')
        mapsf.write('''        }\n''')
        mapsf.write('''        a++;\n''')
        mapsf.write('''        if (a > 360) {\n''')
        mapsf.write('''            a = 1;\n''')
        mapsf.write('''        }\n''')
        mapsf.write('''    }\n''')
        mapsf.write('''    if (direction == 'counterclockwise') {\n''')
        mapsf.write('''        points = points.reverse();\n''')
        mapsf.write('''    }\n''')
        mapsf.write('''    return points;\n''')
        mapsf.write('''}\n''')
        mapsf.write('''\n''')
        mapsf.write('''google.maps.event.addDomListener(window, 'load', initialize);\n''')        
        mapsf.write('''\n''')
        mapsf.write('''</script>\n''')
        mapsf.write('''</body>\n''')
        mapsf.write('''</html>\n''')
        mapsf.close()            
          
        #Remove the temp.csv file we made earlier    
        os.remove(f)
           
  
        if self.checkBox_2.isChecked():            
            os.startfile(str(savemapsf))

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = Map_CDR()
    window.show()
    sys.exit(app.exec_())