from __future__ import division
import sys, os
from PyQt4 import QtCore, QtGui, uic
import hashlib
import time

qtCreatorFile = "ufd_.ui" # Enter file here.


Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

   
        
class MyApp(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.browse_phone.clicked.connect(self.phone_file)
        self.browse_sd.clicked.connect(self.sd_file)
        self.create_ufd.clicked.connect(self.ufd_creator)
        self.cancel.clicked.connect(QtCore.QCoreApplication.instance().quit)
  
      
    def phone_file(self):
        phonef = QtGui.QFileDialog.getOpenFileName()
        self.phone_disp_box.setText(phonef)
        fname = phonef
        
    
    def sd_file(self):
        sdimage = QtGui.QFileDialog.getOpenFileName()
        self.sd_disp_box.setText(sdimage)

    
    def progressc(self):
        self.dialog = Progress()
        self.dialog.closeEvent()    
        
        
    def ufd_creator(self):
        count = 0
        while count == 0:
            
            def md5(name):
                hash_md5 = hashlib.md5()
                with open(name, "rb") as f:
                    for chunk in iter(lambda: f.read(4096), b""):
                        hash_md5.update(chunk)
                    return hash_md5.hexdigest()
            def sha256(name):
                hash_sha256 = hashlib.sha256()
                with open(name, "rb") as f:
                    for chunk in iter(lambda: f.read(4096), b""):
                        hash_sha256.update(chunk)
                    return hash_sha256.hexdigest()      
        
            make = str(self.make.text())
            model = str(self.model.text())
            fname = str(self.phone_disp_box.text())
            sdcard = str(self.sd_disp_box.text())
            method = str(self.comboBox.currentText())
            out_path = str(fname).rsplit('/', 1)
            sd_path = str(sdcard).rsplit('/', 1)
            phone_file_name = out_path[1]
            out_path = out_path[0]+'/'
        
            
            if self.Phone_checkBox.isChecked():
                md5_hash_fname = md5(fname)
                md5_hash_fname = phone_file_name + " = " + md5_hash_fname
                sha256_hash_fname = sha256(fname)
                sha256_hash_fname = phone_file_name + " = " + sha256_hash_fname
             
            else:
                md5_hash_fname = ""
                sha256_hash_fname = ""           
        
            if sdcard != "":
                sd_file_ = sd_path[1]
                sd_file_name = "Image#1=" + sd_file_
            else:
                sd_file_name = ""
                    
            if self.Phone_checkBox_2.isChecked():
                md5_hash_sdcard = md5(sdcard)
                md5_hash_sdcard = sd_file_ + " = " + md5_hash_sdcard
                sha256_hash_sdcard = sha256(sdcard)
                sha256_hash_sdcard = sd_file_ + " = " + sha256_hash_sdcard
            
            else:
                md5_hash_sdcard = fname = ""
                sha256_hash_sdcard = ""           
            
        
            file_data = '[Dumps]\n\
            Image=%s\n\
            %s\n\n\
            [General]\n\
            Device=ANDROID_GENERIC\n\
            FullName=%s_%s\n\
            ExtractionType=Physical\n\
            ExtractionMethod=ANDROID_ADB\n\
            UfdVer=1.2\n\
            ExtractionSoftwareVersion=5.3.6.7\n\
            IsEncrypted=False\n\
            IsEncryptedBySystem=False\n\n\
            [MD5]\n\
            %s\n\
            %s\n\n\
            [SHA256]\n\
            %s\n\
            %s\n\n\
            [notes]\n\
            %s Extraction' % (phone_file_name, sd_file_name, make, model, md5_hash_fname, md5_hash_sdcard, sha256_hash_fname, sha256_hash_sdcard, method)
        
            fout = open(out_path + make +'_' + model + '.ufd', 'wb')
            
            fout.write(file_data)
            fout.close()
          
            count =+ 1
        else: 
            sys.exit() #(app.exec_())
        
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())